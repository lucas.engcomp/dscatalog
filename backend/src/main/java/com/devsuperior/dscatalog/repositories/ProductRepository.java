package com.devsuperior.dscatalog.repositories;

import com.devsuperior.dscatalog.entities.Category;
import com.devsuperior.dscatalog.entities.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

//    @Query("SELECT prod FROM Product prod WHERE " +
//            ":category IN (prod.categories)")
//    Page<Product> find(Category category, Pageable pageable);

//    @Query("SELECT prod FROm Product prod " +
//            "INNER JOIN prod.categories catprod " +
//            "WHERE :category IN (catprod)")
//    Page<Product> find(Category category, Pageable pageable);

//    @Query("SELECT DISTINCT prod FROm Product prod " +
//            "INNER JOIN prod.categories catprod " +
//            "WHERE :category IS NULL OR :category IN (catprod)")
//    Page<Product> find(Category category, Pageable pageable);

    @Query("SELECT DISTINCT prod FROM Product prod " +
            "INNER JOIN prod.categories catprod " +
            "WHERE (COALESCE(:categories) IS NULL OR catprod IN :categories)" +
            "AND (LOWER(prod.name) LIKE LOWER(CONCAT('%', :name, '%')))")
    Page<Product> find(List<Category> categories, String name, Pageable pageable);
}
