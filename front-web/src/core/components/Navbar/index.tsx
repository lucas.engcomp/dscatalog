import React, { useEffect, useState } from 'react';
import { getAccessTokenDecoded, logout } from 'core/utils/auth';
import { Link, NavLink, useLocation } from 'react-router-dom';
import './styles.scss';

const Navbar = () => {

    const [currentUser, setCurrentUser] = useState('');
    const location = useLocation();

    useEffect(() => {
        const currentUserData = getAccessTokenDecoded();
    }, []);

    const handleLogout = (event: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
        event.preventDefault();
        logout();
    }

    return (
        <nav className="row bg-primary main-nav">
            <div className="col-2">
                <Link to="/" className="nav-log-text">
                    <h4>DS Catalog</h4>
                </Link>
            </div>
            <div className="col-6">
                <ul className="main-menu">
                    <li>
                        <NavLink exact to="/" activeClassName="active">HOME</NavLink>
                    </li>
                    <li>
                        <NavLink to="/products" activeClassName="active">CATÁLOGO</NavLink>
                    </li>
                    <li>
                        <NavLink to="/admin" activeClassName="active">ADMIN</NavLink>
                    </li>
                </ul>
            </div>
            <div className="col-3 text-light">
                {currentUser && (
                    <>
                        {currentUser}
                        <a
                            href="#logout"
                            className="nav-link active"
                            onClick={handleLogout}
                        >
                            Logout
                        </a>
                    </>
                )}
                {!currentUser && (
                    <>
                        <Link to="/auth/login" className="nav-link active">
                            Login
                        </Link>
                    </>
                )}
            </div>
        </nav>
    )
};

export default Navbar;
